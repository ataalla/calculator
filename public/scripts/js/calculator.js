var calculationLine = "";


/**
 * this function will show calculation line into the box
 */
function showCalculationLineIntoDisplayBox() {
    $("#calculator-line").html(calculationLine);
}

/**
 * this function will add new character to calculation line
 * @param value
 */
function addCharToCalculationLine(value) {
    if (calculationLine == "0") {
        calculationLine = "";
    }
    calculationLine += value;
    showCalculationLineIntoDisplayBox();
}

/**
 * shows the return value received by server in the result view
 * @param data
 */
function showReturn(data) {
    $("#result-line").show();
    $("#result-line").html(data);
}

/**
 * called when user click on calculator button
 */
function calculateButtonClicked() {

    // clear and hide the result box
    $("#result-line").hide();
    $("#result-line").html('');
    $("#result-line").removeClass('error');

    $.post("/calculate", {
        line: calculationLine,
        _token: csrf
    })
        .done(function (data) {
            console.log(data);
            // load the parsed json
            var parsedData = JSON.parse(data);
            if (parsedData.Error) {
                // it is failed value, show the error message
                showReturn(parsedData.Error);
                $("#result-line").addClass('error');
            } else {
                // it was successful, show the value
                showReturn(parsedData.Value);

            }
        })
        .fail(function () {
            showReturn("An error occurred...");
            $("#result-line").addClass('error');
        });
}


$("#btn-clear").click(function () {

    // clear and hide the result box
    $("#result-line").hide();
    $("#result-line").html('');

    // clear the calculation line
    calculationLine = "0";
    showCalculationLineIntoDisplayBox();
})

$("#btn-0").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(0);
})

$("#btn-1").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(1);
})

$("#btn-2").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(2);
})


$("#btn-3").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(3);
})


$("#btn-4").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(4);
})


$("#btn-5").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(5);
})


$("#btn-6").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(6);
})


$("#btn-7").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(7);
})


$("#btn-8").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(8);
})


$("#btn-9").click(function () {
    // add number to the calculation line
    addCharToCalculationLine(9);
})


$("#btn-plus").click(function () {
    addCharToCalculationLine("+");
})

$("#btn-minus").click(function () {
    addCharToCalculationLine("-");
})

$("#btn-division").click(function () {
    addCharToCalculationLine("/");
})

$("#btn-multiplication").click(function () {
    addCharToCalculationLine("*");
})


$("#btn-calc").click(function () {
    // user requested to calculate the value, we will send data to server
    // and show the result to the user
    calculateButtonClicked();
})

function removeLastCharacter() {
    if (calculationLine.length > 1) {
        text.slice(0, -1)
    } else {
        calculationLine = 0;
    }

}

$('body').keypress(function (e) {
    console.log(e.which);

    if (e.which == 13) {
        calculateButtonClicked();
    }


    if (e.which == 43) {
        addCharToCalculationLine("+");
    }

    if (e.which == 45) {
        addCharToCalculationLine("-");
    }

    if (e.which == 42) {
        addCharToCalculationLine("*");
    }

    if (e.which == 47) {
        addCharToCalculationLine("/");
    }


    if (e.which == 48) {
        addCharToCalculationLine(0);
    }
    if (e.which == 49) {
        addCharToCalculationLine(1);
    }
    if (e.which == 50) {
        addCharToCalculationLine(2);
    }
    if (e.which == 51) {
        addCharToCalculationLine(3);
    }
    if (e.which == 52) {
        addCharToCalculationLine(4);
    }
    if (e.which == 53) {
        addCharToCalculationLine(5);
    }
    if (e.which == 54) {
        addCharToCalculationLine(6);
    }
    if (e.which == 55) {
        addCharToCalculationLine(7);
    }
    if (e.which == 56) {
        addCharToCalculationLine(8);
    }
    if (e.which == 57) {
        addCharToCalculationLine(9);
    }


    // backspace key
    if (e.which == 8) {
        removeLastCharacter();
    }

});
