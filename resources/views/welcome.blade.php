<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Welcome to MR & MISS Smith Calculator</title>

    <!-- Bootstrap core CSS -->
    <link href="/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">


    <style>

        #open-button {
            background: #d53439 !important;
            font-weight: normal !important;
            margin-top: 50px;
            border: 1px solid #6a0b0d !important;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

</head>
<body class="d-flex h-100 text-center text-white bg-dark">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="mb-auto">

    </header>

    <main class="px-3">
        <img style="width: 800px"
             src="https://www.mrandmrssmith.com/v2877dfc35f8/bundles/familysystem/images/MMS_Logo_Tavel_Club_large.svg"/>
        <br/>
        <p class="lead">Welcome to our website, click the calculator button to start calculate travel cost.</p>
        <p class="lead">
            <a id="open-button" href="/calculator" class="btn btn-lg btn-secondary fw-bold border-white bg-white">Open
                Calculator</a>
        </p>
    </main>

    <footer class="mt-auto text-white-50">
        <p>Code By Ata Zangene <span>&#128151;</span></p>
    </footer>
</div>


</body>
</html>
