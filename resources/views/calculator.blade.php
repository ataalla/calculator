<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Welcome to MR & MISS Smith Calculator</title>

    <!-- Bootstrap core CSS -->
    <link href="/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@200;900&display=swap" rel="stylesheet">
    <script src="components/jQuery/dist/jquery.min.js"></script>
    <link href="/style/css/calculator.css" rel="stylesheet">


</head>
<body class="d-flex h-100 text-center text-white bg-dark">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="mb-auto">

    </header>

    <main class="px-3">
        <div class="calculator-box">
            <div class="display-box">
                <div id="calculator-line">
                    0
                </div>
                <div id="result-line">

                </div>
            </div>
            <div class="operators">
                <div id="btn-plus" class="button">
                    +
                </div>
                <div id="btn-minus" class="button">
                    -
                </div>
                <div id="btn-multiplication" class="button">
                    *
                </div>
                <div id="btn-division" class="button">
                    /
                </div>
            </div>
            <div class="numbers-box">
                <div id="btn-7" class="button">
                    7
                </div>
                <div id="btn-8" class="button">
                    8
                </div>
                <div id="btn-9" class="button">
                    9
                </div>
                <div id="btn-4" class="button">
                    4
                </div>
                <div id="btn-5" class="button">
                    5
                </div>
                <div id="btn-6" class="button">
                    6
                </div>
                <div id="btn-1" class="button">
                    1
                </div>
                <div id="btn-2" class="button">
                    2
                </div>
                <div id="btn-3" class="button">
                    3
                </div>
                <div id="btn-clear" class="button">
                    C
                </div>
                <div id="btn-0" class="button">
                    0
                </div>
                <div id="btn-calc" class="button">
                    =
                </div>
            </div>
        </div>

    </main>

    <footer class="mt-auto text-white-50">
        <p>Code By Ata Zangene <span>&#128151;</span></p>
    </footer>
</div>


</body>
</html>
<script>
    var csrf = "{{csrf_token()}}";
</script>
<script src="scripts/js/calculator.js"></script>
