<?php

namespace Tests\Unit;

use App\Classes\Calculator\Calculator;
use App\Classes\Calculator\Operators\DivisionOperator;
use App\Classes\Calculator\Operators\MinusOperator;
use App\Classes\Calculator\Operators\MultiplicationOperator;
use App\Classes\Calculator\Operators\PlusOperator;
use App\Exceptions\Calculator\EmptyCalculateDataException;
use App\Exceptions\Calculator\InvalidOperatorPositionException;
use App\Exceptions\Calculator\InvalidTwoOperatorAfterEachOther;
use App\Exceptions\Calculator\OperatorNotSupportedException;
use App\Models\CalculatorMachine;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * check simple number calculate
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_sample_number()
    {

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $value = $calculator->Calculate("36*2/2+1-1");

        $this->assertEquals(36, $value);
    }


    /**
     * check when we have division by zero in our request
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_divide_by_zero()
    {
        $this->expectException(\DivisionByZeroError::class);

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $calculator->Calculate("15*5+17+5/0+1");
    }

    /**
     * check when we have invalid operator position
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_invalid_operator_position()
    {
        $this->expectException(InvalidOperatorPositionException::class);

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $calculator->Calculate("7+5+");
    }


    /**
     * check when we have two operator after each other
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_invalid_two_operator_after_each_other()
    {
        $this->expectException(InvalidTwoOperatorAfterEachOther::class);

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $calculator->Calculate("23++15");
    }


    /**
     * check when we pass single number to the calculator
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_single_number()
    {
        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $value = $calculator->Calculate("151");

        $this->assertEquals(151, $value);
    }

    /**
     * check we pass only one single operator
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_only_symbol()
    {
        $this->expectException(InvalidOperatorPositionException::class);

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $calculator->Calculate("*");

    }

    /**
     * check if we do not pass anything to the calculator
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_empty_data()
    {
        $this->expectException(EmptyCalculateDataException::class);

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $calculator->Calculate("");

    }


    /**
     * check if the calculator checks for leading operators like * and / first
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_leading_operators()
    {

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $value = $calculator->Calculate("25*2+3");
        $this->assertEquals(53, $value);

    }

    /**
     * check if we pass not supported operator
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_operator_not_supported()
    {
        $this->expectException(OperatorNotSupportedException::class);

        // make a new calculator and send back to the list
        $calculator = new CalculatorMachine();
        $calculator->Calculate("25^2+3");
    }


    /**
     * make a new calculator directly
     * @return void
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function test_direct_calculate()
    {
        // make a new calculator and send back to the list
        $calculator = new Calculator();

        // make an operation list
        $operators = array();
        $operators[] = new PlusOperator();
        $operators[] = new MinusOperator();
        $operators[] = new DivisionOperator();
        $operators[] = new MultiplicationOperator();
        $calculator->setSupportedOperators($operators);

        // make a calculation and send back the result
        $value = $calculator->Calculate("12*3", $operators);
        $this->assertEquals(36, $value);
    }


    /**
     * check if we can set and get supported operators
     * @return void
     */
    public function test_set_supported_operator()
    {
        // make a new calculator and send back to the list
        $calculator = new Calculator();

        // make an operation list
        $operators = array();
        $operators[] = new PlusOperator();
        $operators[] = new MinusOperator();
        $operators[] = new DivisionOperator();
        $operators[] = new MultiplicationOperator();
        $calculator->setSupportedOperators($operators);

        // make a calculation and send back the result
        $this->assertEquals(4, count($calculator->getSupportedOperators()));
    }
}
