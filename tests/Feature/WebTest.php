<?php

namespace Tests\Feature;

use Tests\TestCase;

class WebTest extends TestCase
{
    /**
     * test if the home page is working well
     *
     * @return void
     */
    public function test_website_home_page_working()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * test of the calculator page working
     *
     * @return void
     */
    public function test_calculator_page_working()
    {
        $response = $this->get('/calculator');

        $response->assertStatus(200);
    }


    /**
     * test of the calculate page working
     *
     * @return void
     */
    public function test_calculate_page_working()
    {
        $response = $this->post('/calculate', array(
            'line' => '25*6'
        ));

        $response->assertStatus(200);
    }

}
