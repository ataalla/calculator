<h1>Welcome to the MR & MRS Smith Calculator</h1> 
In this repository, we have simple calculator that can calculate the numbers based on the given data. At this moment, it only do following operators :
plus, minus, division and multiplication. This website written in Laravel framework

<h4>How to setup this website</h4>
To setup this website, All you need is to copy and paste this website under a virtual machine on your system or server
 
<h4>How to add new operator</h4>
To make a new operator, you need to extend a new class of BaseOperation and then add symbol and calculation to their methods. After that, in <b>Calculator.php</b> model, add this operator as a new parameter

<h4>Tests ( Unit Testing, Feature Testing)</h4>
There are more than 10 test was written for this website, As this website written in Laravel framework, to run the test, run this command at main root of the directory
<br/>
<br/>
<pre>php artisan test</pre> 


<h4>Tips</h4>
<ul>
<li>
As this test was for backend developer, we did not write any test for front end ( javascript )
</li>
<li>
I have tried to follow SOLID And TDD principal during writing codes.  
</li>
<li>
You can use keyborad while testing the calculator
</li>
<li>
The calculator do not support floating number input at this moment. But we can add that if you need to  
</li>
</ul>


