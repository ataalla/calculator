<?php

namespace App\Models;

use App\Classes\Calculator\Calculator;
use App\Classes\Calculator\Operators\DivisionOperator;
use App\Classes\Calculator\Operators\MinusOperator;
use App\Classes\Calculator\Operators\MultiplicationOperator;
use App\Classes\Calculator\Operators\PlusOperator;
use App\Exceptions\Calculator\EmptyCalculateDataException;
use App\Exceptions\Calculator\InvalidOperatorPositionException;
use App\Exceptions\Calculator\InvalidTwoOperatorAfterEachOther;
use App\Exceptions\Calculator\OperatorNotSupportedException;

class CalculatorMachine
{
    /**
     * calculate the parameter and send back the result as float number
     * @param $calculationLine
     * @return false|float
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function Calculate($calculationLine)
    {

        // make a new calculator and send back to the list
        $calculator = new Calculator();

        // make an operation list
        $operators = array();
        $operators[] = new PlusOperator();
        $operators[] = new MinusOperator();
        $operators[] = new DivisionOperator();
        $operators[] = new MultiplicationOperator();
        $calculator->setSupportedOperators($operators);

        // make a calculation and send back the result
        return $calculator->Calculate($calculationLine, $operators);

    }
}
