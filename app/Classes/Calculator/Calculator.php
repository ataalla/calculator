<?php


namespace App\Classes\Calculator;


use App\Exceptions\Calculator\EmptyCalculateDataException;
use App\Exceptions\Calculator\InvalidOperatorPositionException;
use App\Exceptions\Calculator\InvalidTwoOperatorAfterEachOther;
use App\Exceptions\Calculator\OperatorNotSupportedException;

class Calculator
{

    private array $supportedOperators;

    /**
     * calculate the line parameter based on the operators received
     * @param $line
     * @return false|float
     * @throws EmptyCalculateDataException
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    public function Calculate($line)
    {
        // first, we need to check the line for error
        if (strlen($line) == 0) {
            throw new EmptyCalculateDataException("Calculation data can not be empty");
        }

        // then, we need to parse them
        $numberOperationList = $this->ParseTheCalculationLine($line);

        // now, we need to calculate the result based on the leading operators
        $this->calculateTheLeadingOperators($numberOperationList, true);

        // now, we need to check all items
        $this->calculateTheLeadingOperators($numberOperationList, false);

        // now, we have to send back the first one item in array which is result
        return round($numberOperationList[0], 5);


    }

    /**
     * this function will parse the requested calculation line and send back result
     * @param $line
     * @return array list of items based on the request
     * @throws InvalidOperatorPositionException
     * @throws InvalidTwoOperatorAfterEachOther
     * @throws OperatorNotSupportedException
     */
    private function ParseTheCalculationLine($line)
    {
        $pos = 0;
        $lastNumber = "";
        $numberOperationList = array();
        while ($pos < strlen($line)) {
            $character = substr($line, $pos, 1);

            if (is_numeric($character) === false) {

                // we will check if the last number is not zero length,
                // then add as a number and
                if (strlen($lastNumber) > 0) {
                    $numberOperationList[] = floatval($lastNumber);
                    $lastNumber = "";
                }

                // the correct character is not number, we have to check if the character exists in character list
                if (!$this->checkCharacterIsSupported($character, $this->getSupportedOperators())) {
                    throw new OperatorNotSupportedException(sprintf("The operator of %s is not supported at this moment", $character));
                }


                if (count($numberOperationList) == 0 || $pos == strlen($line) - 1) {
                    throw new InvalidOperatorPositionException(sprintf("The calculation can not start or finish by %s", $character));
                }

                // check if the last item is symbol by itself
                if (count($numberOperationList) > 0
                    && !is_numeric($numberOperationList[count($numberOperationList) - 1])) {
                    throw new InvalidTwoOperatorAfterEachOther("You can not use two operator after each other");
                }

                // this symbol was found in the operators, now add that to operation list
                $numberOperationList[] = $character;

            } else {
                $lastNumber .= $character;
            }

            $pos++;
        }

        // we have reached the end, but we need to find the last number if exists
        if (strlen($lastNumber) > 0) {
            $numberOperationList[] = floatval($lastNumber);
        }

        return $numberOperationList;

    }

    /**
     * this function will check for the character to know it is supported by the operators
     * @param $symbol it should be a operator symbol like +
     * @return bool return true if the symbol was found in the operators
     */
    private function checkCharacterIsSupported($symbol)
    {

        // check if item is found
        foreach ($this->getSupportedOperators() as $operator) {
            if ($operator->GetOperatorSymbol() === $symbol) {
                return true;
            }
        }

        // item not found in operators list
        return false;
    }


    /**
     * this function will calculate the leading operators
     * @param $numberOperationList
     * @param $leadingOperatorsOnly if this parameter became true, it will check for the leading operator only
     */
    private function calculateTheLeadingOperators(&$numberOperationList, $leadingOperatorsOnly)
    {
        $returnList = array();
        $pos = 0;
        while ($pos < count($numberOperationList)) {

            // get the item
            $item = $numberOperationList[$pos];
            if (!is_numeric($item)) {

                for ($i = 0; $i < count($this->getSupportedOperators()); $i++) {
                    if ($item == $this->getSupportedOperators()[$i]->GetOperatorSymbol() &&
                        (($leadingOperatorsOnly && $this->getSupportedOperators()[$i]->IsLeadingOperator()) || $leadingOperatorsOnly == false)) {

                        // calculate the value
                        $this->getSupportedOperators()[$i]->setLeftNumber($numberOperationList[$pos - 1]);
                        $this->getSupportedOperators()[$i]->setRightNumber($numberOperationList[$pos + 1]);
                        $value = $this->getSupportedOperators()[$i]->CalculateTheValue();

                        // make a new array
                        $newValueAsArray = array();
                        $newValueAsArray[] = $value;
                        array_splice($numberOperationList, $pos - 1, 3, $newValueAsArray);

                        // make the position to lower again
                        $pos -= 1;
                    }
                }
            } else {
                $returnList[] = $item;
            }
            $pos++;
        }

    }

    /**
     * send back list of supported operators
     * @return array
     */
    public function getSupportedOperators(): array
    {
        return $this->supportedOperators;
    }

    /**
     * set supported operators
     * @param array $supportedOperators
     */
    public function setSupportedOperators(array $supportedOperators): void
    {
        $this->supportedOperators = $supportedOperators;
    }

}
