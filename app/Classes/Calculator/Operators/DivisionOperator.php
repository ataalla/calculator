<?php


namespace App\Classes\Calculator\Operators;


class DivisionOperator extends BaseOperation
{

    function GetOperatorSymbol(): String
    {
        return "/";
    }

    function CalculateTheValue()
    {

        if (floatval($this->getRightNumber()) == 0) {
            // invalid operation
            throw new \DivisionByZeroError("You can not divide number by zero");
        }

        return $this->getLeftNumber() / $this->getRightNumber();
    }

    /**
     * @inheritDoc
     */
    function IsLeadingOperator()
    {
        return true;
    }
}
