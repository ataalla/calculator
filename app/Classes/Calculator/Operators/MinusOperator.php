<?php


namespace App\Classes\Calculator\Operators;


class MinusOperator extends BaseOperation
{

    function GetOperatorSymbol() : String
    {
        return "-";
    }

    function CalculateTheValue()
    {
        return $this->getLeftNumber() - $this->getRightNumber();
    }

    /**
     * @inheritDoc
     */
    function IsLeadingOperator()
    {
        return false;
    }
}
