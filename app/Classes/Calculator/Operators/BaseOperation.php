<?php


namespace App\Classes\Calculator\Operators;


abstract class BaseOperation
{

    private float $leftNumber;
    private float $rightNumber;

    /**
     * @return mixed
     */
    public function getLeftNumber()
    {
        return $this->leftNumber;
    }

    /**
     * @return mixed
     */
    public function getRightNumber()
    {
        return $this->rightNumber;
    }

    /**
     * @param mixed $leftNumber
     */
    public function setLeftNumber($leftNumber)
    {
        $this->leftNumber = $leftNumber;
    }

    /**
     * @param mixed $rightNumber
     */
    public function setRightNumber($rightNumber)
    {
        $this->rightNumber = $rightNumber;
    }

    /**
     * this function will send back the calculation operator symbol as string
     * @return mixed
     */
    abstract function GetOperatorSymbol(): String;

    /**
     * this function will calculate the value based on the operator name
     * @return mixed
     */
    abstract function CalculateTheValue();

    /**
     * some operators like * and / should be calculated first
     * @return mixed
     */
    abstract function IsLeadingOperator();
}
