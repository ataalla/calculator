<?php

namespace App\Http\Controllers;

use App\Models\CalculatorMachine;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CalculatorController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * this function behind the calculation page route, so when user click on Open Calculator
     * button on the home page, this route will open
     */
    public function Calculator()
    {
        // set the view for this route
        return view('calculator');
    }


    /**
     * this route will calculate the request and send back as json
     * @param Request $request
     * @return false|string
     */
    public function Calculate(Request $request)
    {

        // make a result for sending
        $result = new \stdClass();

        // fetch the calculation line from POST data
        $calculationLine = $request->get("line");
        if (empty($calculationLine)) {
            $result->Error = "Empty calculation line received";
            return json_encode($result);
        }

        // make a new calculation machine model
        $calculatorMachine = new CalculatorMachine();

        try {
            // calculate the value
            $value = $calculatorMachine->Calculate($calculationLine);
            $result->Value = $value;
        } catch (\Throwable $e) {
            $result->Error = $e->getMessage();
        }

        // send back the result
        return json_encode($result);

    }
}
